# Benutzergruppen

1. Variante 3 Gruppen
      i) Besucher
     ii) Stammspieler
    iii) Bewohner
     iv) Mieter 
      v) Owner (ruhig mehrere)
    
    
Protection /protect
- An/Abbauen von Blöcken (nach Kategorien, auch Platzieren von Redstone)
 Kategorien:
  - nach Config
- Öffnen/Schließen von Türen (Holz)
  - nur Holztüren
- Öffnen/Schließen von Falltüren
- Öffnen/Schließen von Zauntoren

- Nutzen von Redstone (unterscheiden von Holz und Stein)
  - "Aktvierer" (Knöpfe/Druckplatten)
  - Editieren von Redstoneblöcken (Comperator/Repeater)
- Öffnen von Kisten/Öfen/Brauständen/... (für alles)
- Nutzen von Kisten/Öfen/Brauständen/... (unterteilt)

Nutzer kann nur Blöcke platzieren und Redstone nutzen aber nicht auf Inventare zugreifen
Was passiert wenn Hopper unter Kiste in Dropper mit Clock führt?

- Wasser + Lava setzen und wegnehmen
-> exclude bei Blöcke setzen
- Feuerzeug nutzen (Item nutzen) -> für jedes Werkzeug individuell
(- Einsteigen von Fahrzeugen; Aussteigen -> gleich ins Inventar)
 Gesamte Welt geschützt
  - einzelne Optionen für global einstellen
Frage: Verhalten Kistenminecarts? oder generell Entitys

- Tiere zähmen/schlachten/vermehren
  - Hühnereier beachten
  
werfbare Entitäten? einstellbar
  
- Creeperschaden von Gästen?

Creeper/TNT am Rand des Grundstücks

Witherspawning?



Grundstücke /plot
- Grundstück erstellen (mit Item, ausschaltbar) (vorschau beim erstellen, muss bestätigen)
- vergrößeren (auch selber, dann aber nur bestimmte Anzahl)
- verkleinern (ohne Rückerstattung)
- Grundstücke übertragen (doppelte Bestätigung)
    anderen Nutzer als Owner setzen
    sich selber austragen -> funktioniert nicht, wenn kein anderer Owner -> macht dann Mod
- Grundstücke löschen (nur für Mods)
