package dev.mfischer.minecraft.mcestate

import dev.mfischer.minecraft.mcestate.commands.PlotManageCommand
import dev.mfischer.minecraft.mcestate.listeners.InventoryListener
import org.bukkit.plugin.java.JavaPlugin

class MCEstate : JavaPlugin() {

    override fun onEnable() {
        getCommand("manage")?.setExecutor(PlotManageCommand())
        logger.info("Enabling dev.mfischer.minecraft.mcestate.MCEstate.MCEstate...")
        server.pluginManager.registerEvents(InventoryListener(), this)
    }
}