package dev.mfischer.minecraft.mcestate.listeners

import dev.mfischer.minecraft.mcestate.ui.MenuChestInventory
import dev.mfischer.minecraft.mcestate.ui.MenuInventoryHolder
import org.bukkit.Material
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.inventory.InventoryClickEvent
import org.bukkit.event.inventory.InventoryDragEvent
import org.bukkit.inventory.ItemStack
import java.util.*

class InventoryListener : Listener {

    @EventHandler
    fun onClickEvent(event: InventoryClickEvent) {
        if (event.inventory.holder is MenuInventoryHolder) {
            event.isCancelled = true
            val holder = event.inventory.holder as MenuInventoryHolder

            if (holder.menu is MenuChestInventory) {
                val menu = holder.menu as MenuChestInventory
                if (menu.toggleItems.containsKey(event.rawSlot)) {
                    val oldStatus = menu.toggleStatus[event.rawSlot] ?: true
                    menu.toggleStatus[event.rawSlot] = !oldStatus
                    menu.toggleCallbacks[event.rawSlot]?.invoke(!oldStatus)
                    val item_pair = menu.toggleItems[event.rawSlot] ?: Pair(ItemStack(Material.BARRIER, 1),ItemStack(Material.BARRIER, 1))
                    menu.inventory.setItem(event.rawSlot, if (oldStatus) item_pair.second else item_pair.first)
                } else if (menu.menuSwitch.containsKey(event.rawSlot)) {
                    holder.player.openInventory(menu.menuSwitch[event.rawSlot]!!.inventory)
                }
            }

        }
    }

    @EventHandler
    fun onDragEvent(event: InventoryDragEvent) {

    }
}