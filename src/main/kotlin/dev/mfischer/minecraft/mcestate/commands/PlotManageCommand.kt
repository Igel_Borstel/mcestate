package dev.mfischer.minecraft.mcestate.commands

import dev.mfischer.minecraft.mcestate.ui.MenuChestInventory
import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack

class PlotManageCommand : CommandExecutor {

    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (label == "manage") {
            if (sender is Player) {
                val next_page = MenuChestInventory(sender, 27, "Seite 2")
                next_page.setItem(13, ItemStack(Material.DIAMOND_BLOCK, 64))

                val ui_inv = MenuChestInventory(sender, 9, "Testmenu")
                ui_inv.setItem(3, ItemStack(Material.GRASS_BLOCK, 1))
                ui_inv.setToggleItem(4, ItemStack(Material.IRON_SHOVEL, 1), callback = {active: Boolean -> Bukkit.getLogger().info("Activity: " + active)})
                ui_inv.setMenuSwitchItem(6, ItemStack(Material.DIAMOND, 1), menu = next_page)
                sender.openInventory(ui_inv.inventory)
            } else {
                sender.sendMessage("Huh. You don't look like a player. But only players can use an inventory!")
            }

            return true
        }
        return false
    }
}