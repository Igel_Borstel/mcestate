package dev.mfischer.minecraft.mcestate.ui

import org.bukkit.entity.Player
import org.bukkit.inventory.Inventory
import org.bukkit.inventory.InventoryHolder

open class MenuInventoryHolder(var menu: MenuInventory, var player: Player) : InventoryHolder {
    override fun getInventory(): Inventory = menu.inventory
}