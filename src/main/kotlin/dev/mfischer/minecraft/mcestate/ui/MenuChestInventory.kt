package dev.mfischer.minecraft.mcestate.ui

import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.entity.Player
import org.bukkit.inventory.Inventory
import org.bukkit.inventory.ItemStack

class MenuChestInventory : MenuInventory {
    override var inventory: Inventory

    var toggleItems: MutableMap<Int, Pair<ItemStack, ItemStack>> = mutableMapOf()
    var toggleCallbacks: MutableMap<Int, (active: Boolean) -> Unit> = mutableMapOf()
    var toggleStatus: MutableMap<Int, Boolean> = mutableMapOf()

    var menuSwitch: MutableMap<Int, MenuInventory> = mutableMapOf()

    constructor(player: Player, size: Int, title: String) : super() {
        inventory = Bukkit.createInventory(MenuInventoryHolder(this, player), size, title)
    }

    fun setItem(slot: Int, item: ItemStack) {
        if (slot >= inventory.size) {
            throw IndexOutOfBoundsException()
        }
        inventory.setItem(slot, item)
    }

    fun setItem(x: Int, y: Int, item: ItemStack) = setItem(y * 9 + x, item)

    fun setToggleItem(slot: Int, item_active: ItemStack, item_inactive: ItemStack = ItemStack(Material.BARRIER, 1),
                      active: Boolean = true, callback: (active: Boolean) -> Unit) {
        if (active) {
            setItem(slot, item_active)
        } else {
            setItem(slot, item_inactive)
        }
        toggleItems[slot] = Pair(item_active, item_inactive)
        toggleCallbacks[slot] = callback
        toggleStatus[slot] = active
    }

    fun setToggleItem(x: Int, y: Int, item_active: ItemStack, item_inactive: ItemStack = ItemStack(Material.BARRIER, 1),
                      active: Boolean = true, callback: (active: Boolean) -> Unit)
        = setToggleItem(y * 9 + x, item_active, item_inactive, active, callback)

    fun setMenuSwitchItem(slot: Int, item: ItemStack, menu: MenuInventory) {
        setItem(slot, item)
        menuSwitch[slot] = menu
    }
}